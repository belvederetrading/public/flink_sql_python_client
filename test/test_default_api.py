"""
    Fink SQL Rest API

    Flink SQL Rest API  # noqa: E501

    The version of the OpenAPI document: v1
    Generated by: https://openapi-generator.tech
"""


import unittest

import flink_sql_gateway_client
from flink_sql_gateway_client.api.default_api import DefaultApi  # noqa: E501


class TestDefaultApi(unittest.TestCase):
    """DefaultApi unit test stubs"""

    def setUp(self):
        self.api = DefaultApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_info_get(self):
        """Test case for info_get"""
        pass


if __name__ == "__main__":
    unittest.main()
