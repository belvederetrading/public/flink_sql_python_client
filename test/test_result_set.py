"""
    Fink SQL Rest API

    Flink SQL Rest API  # noqa: E501

    The version of the OpenAPI document: v1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import flink_sql_gateway_client
from flink_sql_gateway_client.model.result_column import ResultColumn

globals()["ResultColumn"] = ResultColumn
from flink_sql_gateway_client.model.result_set import ResultSet


class TestResultSet(unittest.TestCase):
    """ResultSet unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testResultSet(self):
        """Test ResultSet"""
        # FIXME: construct object with mandatory attributes with example values
        # model = ResultSet()  # noqa: E501
        pass


if __name__ == "__main__":
    unittest.main()
