"""
    Fink SQL Rest API

    Flink SQL Rest API  # noqa: E501

    The version of the OpenAPI document: v1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import flink_sql_gateway_client
from flink_sql_gateway_client.model.result_set import ResultSet

globals()["ResultSet"] = ResultSet
from flink_sql_gateway_client.model.execute_statement_response import (
    ExecuteStatementResponse,
)


class TestExecuteStatementResponse(unittest.TestCase):
    """ExecuteStatementResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testExecuteStatementResponse(self):
        """Test ExecuteStatementResponse"""
        # FIXME: construct object with mandatory attributes with example values
        # model = ExecuteStatementResponse()  # noqa: E501
        pass


if __name__ == "__main__":
    unittest.main()
