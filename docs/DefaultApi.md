# flink_sql_gateway_client.DefaultApi

All URIs are relative to *http://10.130.0.45:8083/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**info_get**](DefaultApi.md#info_get) | **GET** /info | 


# **info_get**
> InfoResponse info_get()



### Example

```python
import time
import flink_sql_gateway_client
from flink_sql_gateway_client.api import default_api
from flink_sql_gateway_client.model.info_response import InfoResponse
from pprint import pprint
# Defining the host is optional and defaults to http://10.130.0.45:8083/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = flink_sql_gateway_client.Configuration(
    host = "http://10.130.0.45:8083/v1"
)


# Enter a context with an instance of the API client
with flink_sql_gateway_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        api_response = api_instance.info_get()
        pprint(api_response)
    except flink_sql_gateway_client.ApiException as e:
        print("Exception when calling DefaultApi->info_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**InfoResponse**](InfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

