# flink_sql_gateway_client.FlinkSqlApi

All URIs are relative to *http://10.130.0.45:8083/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancel_job**](FlinkSqlApi.md#cancel_job) | **DELETE** /sessions/{session_id}/jobs/{job_id} | Cancel Flink SQL job in session.
[**cancel_session**](FlinkSqlApi.md#cancel_session) | **DELETE** /sessions/{session_id} | Cancel Flink SQL job in session.
[**create_session**](FlinkSqlApi.md#create_session) | **POST** /sessions | Creates a session to run flink sql jobs
[**execute_statement**](FlinkSqlApi.md#execute_statement) | **POST** /sessions/{session_id}/statements | Executes Flink SQL statement in session.
[**get_job_results**](FlinkSqlApi.md#get_job_results) | **GET** /sessions/{session_id}/jobs/{job_id}/result/{token} | Get results of a Flink SQL job.
[**get_job_status**](FlinkSqlApi.md#get_job_status) | **GET** /sessions/{session_id}/jobs/{job_id}/status | Get Flink SQL job status in session.
[**heartbeat_session**](FlinkSqlApi.md#heartbeat_session) | **POST** /sessions/{session_id}/heartbeat | Executes Flink SQL heartbeat to keep session alive.


# **cancel_job**
> JobStatusResponse cancel_job(session_id, job_id)

Cancel Flink SQL job in session.

### Example

```python
import time
import flink_sql_gateway_client
from flink_sql_gateway_client.api import flink_sql_api
from flink_sql_gateway_client.model.job_status_response import JobStatusResponse
from pprint import pprint
# Defining the host is optional and defaults to http://10.130.0.45:8083/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = flink_sql_gateway_client.Configuration(
    host = "http://10.130.0.45:8083/v1"
)


# Enter a context with an instance of the API client
with flink_sql_gateway_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = flink_sql_api.FlinkSqlApi(api_client)
    session_id = "session_id_example" # str | String value that identifies a session.  Create using /session post commang
    job_id = "job_id_example" # str | String value that identifies a job.

    # example passing only required values which don't have defaults set
    try:
        # Cancel Flink SQL job in session.
        api_response = api_instance.cancel_job(session_id, job_id)
        pprint(api_response)
    except flink_sql_gateway_client.ApiException as e:
        print("Exception when calling FlinkSqlApi->cancel_job: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **str**| String value that identifies a session.  Create using /session post commang |
 **job_id** | **str**| String value that identifies a job. |

### Return type

[**JobStatusResponse**](JobStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cancel_session**
> SessionStatusResponse cancel_session(session_id)

Cancel Flink SQL job in session.

### Example

```python
import time
import flink_sql_gateway_client
from flink_sql_gateway_client.api import flink_sql_api
from flink_sql_gateway_client.model.session_status_response import SessionStatusResponse
from pprint import pprint
# Defining the host is optional and defaults to http://10.130.0.45:8083/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = flink_sql_gateway_client.Configuration(
    host = "http://10.130.0.45:8083/v1"
)


# Enter a context with an instance of the API client
with flink_sql_gateway_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = flink_sql_api.FlinkSqlApi(api_client)
    session_id = "session_id_example" # str | String value that identifies a session.  Create using /session post commang

    # example passing only required values which don't have defaults set
    try:
        # Cancel Flink SQL job in session.
        api_response = api_instance.cancel_session(session_id)
        pprint(api_response)
    except flink_sql_gateway_client.ApiException as e:
        print("Exception when calling FlinkSqlApi->cancel_session: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **str**| String value that identifies a session.  Create using /session post commang |

### Return type

[**SessionStatusResponse**](SessionStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_session**
> CreateSessionResponse create_session(create_session_request)

Creates a session to run flink sql jobs

### Example

```python
import time
import flink_sql_gateway_client
from flink_sql_gateway_client.api import flink_sql_api
from flink_sql_gateway_client.model.create_session_request import CreateSessionRequest
from flink_sql_gateway_client.model.create_session_response import CreateSessionResponse
from pprint import pprint
# Defining the host is optional and defaults to http://10.130.0.45:8083/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = flink_sql_gateway_client.Configuration(
    host = "http://10.130.0.45:8083/v1"
)


# Enter a context with an instance of the API client
with flink_sql_gateway_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = flink_sql_api.FlinkSqlApi(api_client)
    create_session_request = CreateSessionRequest(
        session_name="session_name_example",
        planner="blink",
        execution_type="batch",
    ) # CreateSessionRequest | 

    # example passing only required values which don't have defaults set
    try:
        # Creates a session to run flink sql jobs
        api_response = api_instance.create_session(create_session_request)
        pprint(api_response)
    except flink_sql_gateway_client.ApiException as e:
        print("Exception when calling FlinkSqlApi->create_session: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_session_request** | [**CreateSessionRequest**](CreateSessionRequest.md)|  |

### Return type

[**CreateSessionResponse**](CreateSessionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **execute_statement**
> ExecuteStatementResponse execute_statement(session_id, execute_statement_request)

Executes Flink SQL statement in session.

### Example

```python
import time
import flink_sql_gateway_client
from flink_sql_gateway_client.api import flink_sql_api
from flink_sql_gateway_client.model.execute_statement_request import ExecuteStatementRequest
from flink_sql_gateway_client.model.execute_statement_response import ExecuteStatementResponse
from pprint import pprint
# Defining the host is optional and defaults to http://10.130.0.45:8083/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = flink_sql_gateway_client.Configuration(
    host = "http://10.130.0.45:8083/v1"
)


# Enter a context with an instance of the API client
with flink_sql_gateway_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = flink_sql_api.FlinkSqlApi(api_client)
    session_id = "session_id_example" # str | String value that identifies a session.  Create using /session post commang
    execute_statement_request = ExecuteStatementRequest(
        statement="statement_example",
        execution_timeout=1,
    ) # ExecuteStatementRequest | 

    # example passing only required values which don't have defaults set
    try:
        # Executes Flink SQL statement in session.
        api_response = api_instance.execute_statement(session_id, execute_statement_request)
        pprint(api_response)
    except flink_sql_gateway_client.ApiException as e:
        print("Exception when calling FlinkSqlApi->execute_statement: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **str**| String value that identifies a session.  Create using /session post commang |
 **execute_statement_request** | [**ExecuteStatementRequest**](ExecuteStatementRequest.md)|  |

### Return type

[**ExecuteStatementResponse**](ExecuteStatementResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job_results**
> ResultFetchResponse get_job_results(session_id, job_id, token, result_fetch_request)

Get results of a Flink SQL job.

### Example

```python
import time
import flink_sql_gateway_client
from flink_sql_gateway_client.api import flink_sql_api
from flink_sql_gateway_client.model.result_fetch_request import ResultFetchRequest
from flink_sql_gateway_client.model.result_fetch_response import ResultFetchResponse
from pprint import pprint
# Defining the host is optional and defaults to http://10.130.0.45:8083/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = flink_sql_gateway_client.Configuration(
    host = "http://10.130.0.45:8083/v1"
)


# Enter a context with an instance of the API client
with flink_sql_gateway_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = flink_sql_api.FlinkSqlApi(api_client)
    session_id = "session_id_example" # str | String value that identifies a session.  Create using /session post commang
    job_id = "job_id_example" # str | String value that identifies a job.
    token = 1 # int | Token value to get results, If it's the first time the token value must be 0. To fetch the next part of the result, increase token by 1 and call this API again.
    result_fetch_request = ResultFetchRequest(
        max_fetch_size=1,
    ) # ResultFetchRequest | 

    # example passing only required values which don't have defaults set
    try:
        # Get results of a Flink SQL job.
        api_response = api_instance.get_job_results(session_id, job_id, token, result_fetch_request)
        pprint(api_response)
    except flink_sql_gateway_client.ApiException as e:
        print("Exception when calling FlinkSqlApi->get_job_results: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **str**| String value that identifies a session.  Create using /session post commang |
 **job_id** | **str**| String value that identifies a job. |
 **token** | **int**| Token value to get results, If it&#39;s the first time the token value must be 0. To fetch the next part of the result, increase token by 1 and call this API again. |
 **result_fetch_request** | [**ResultFetchRequest**](ResultFetchRequest.md)|  |

### Return type

[**ResultFetchResponse**](ResultFetchResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job_status**
> JobStatusResponse get_job_status(session_id, job_id)

Get Flink SQL job status in session.

### Example

```python
import time
import flink_sql_gateway_client
from flink_sql_gateway_client.api import flink_sql_api
from flink_sql_gateway_client.model.job_status_response import JobStatusResponse
from pprint import pprint
# Defining the host is optional and defaults to http://10.130.0.45:8083/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = flink_sql_gateway_client.Configuration(
    host = "http://10.130.0.45:8083/v1"
)


# Enter a context with an instance of the API client
with flink_sql_gateway_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = flink_sql_api.FlinkSqlApi(api_client)
    session_id = "session_id_example" # str | String value that identifies a session.  Create using /session post commang
    job_id = "job_id_example" # str | String value that identifies a job.

    # example passing only required values which don't have defaults set
    try:
        # Get Flink SQL job status in session.
        api_response = api_instance.get_job_status(session_id, job_id)
        pprint(api_response)
    except flink_sql_gateway_client.ApiException as e:
        print("Exception when calling FlinkSqlApi->get_job_status: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **str**| String value that identifies a session.  Create using /session post commang |
 **job_id** | **str**| String value that identifies a job. |

### Return type

[**JobStatusResponse**](JobStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **heartbeat_session**
> heartbeat_session(session_id)

Executes Flink SQL heartbeat to keep session alive.

### Example

```python
import time
import flink_sql_gateway_client
from flink_sql_gateway_client.api import flink_sql_api
from pprint import pprint
# Defining the host is optional and defaults to http://10.130.0.45:8083/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = flink_sql_gateway_client.Configuration(
    host = "http://10.130.0.45:8083/v1"
)


# Enter a context with an instance of the API client
with flink_sql_gateway_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = flink_sql_api.FlinkSqlApi(api_client)
    session_id = "session_id_example" # str | String value that identifies a session.  Create using /session post commang

    # example passing only required values which don't have defaults set
    try:
        # Executes Flink SQL heartbeat to keep session alive.
        api_instance.heartbeat_session(session_id)
    except flink_sql_gateway_client.ApiException as e:
        print("Exception when calling FlinkSqlApi->heartbeat_session: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **str**| String value that identifies a session.  Create using /session post commang |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200 response          |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

