import logging
from flink_sql_gateway_client.flink_sql_magic import FlinkSQLMagic

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

tester = FlinkSQLMagic(None)
tester.connect_flink_sql("--host 10.130.0.45 --port 8083 --session_name yab_test")
tester.flink_sql(
    "--catalog pulsar --execution_timeout 1000000 --num_fetches 10 --max_fetch_size 1",
    "SHOW CATALOGS",
)
